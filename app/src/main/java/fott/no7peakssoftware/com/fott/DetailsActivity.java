package fott.no7peakssoftware.com.fott;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import org.w3c.dom.Text;

import fott.no7peakssoftware.com.fott.model.Content;
import fott.no7peakssoftware.com.fott.model.Content_Details;


public class DetailsActivity extends Activity {

    public static final String KEY_CONTENT = "content";

    private Context context;
    private ViewGroup viewGroup;
    private ImageView imgThumbnail;
    private TextView txtTitle, txtAuthor, txtIngress, txtType, txtDesc;
    private LinearLayout layoutItem;
    private Content content;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_ACTION_BAR);
        setContentView(R.layout.activity_details);

        getActionBar().setCustomView(R.layout.custom_details_top_bar);
        getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);

        viewGroup = (ViewGroup) getActionBar().getCustomView();
        ImageView btnBack = (ImageView) viewGroup.findViewById(R.id.btnBack);
        TextView textTitle = (TextView) viewGroup.findViewById(R.id.title);

        context = DetailsActivity.this;

        imgThumbnail = (ImageView) findViewById(R.id.imgThumbnail);
        txtTitle = (TextView) findViewById(R.id.txtTitle);
        txtAuthor = (TextView) findViewById(R.id.txtAuthor);
        txtIngress = (TextView) findViewById(R.id.txtIngress);

        layoutItem = (LinearLayout) findViewById(R.id.layoutItem);

        Intent intent = getIntent();
        content = (Content) intent.getSerializableExtra(KEY_CONTENT);

        textTitle.setText(content.title);

        setupComponent(content);

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void setupComponent(Content content) {

        ImageLoader.getInstance().displayImage(content.image, imgThumbnail);

        txtTitle.setText(content.title);
        txtAuthor.setText(content.dateTime);
        txtIngress.setText(content.ingress);

        for(Content_Details content_details : content.contentList){
            layoutItem.addView(generateDetailsItem(content_details));
        }
    }

    private View generateDetailsItem(Content_Details content_details) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.layout_details_item, null);

        TextView txtType = (TextView) view.findViewById(R.id.txtType);
        TextView txtSubject = (TextView) view.findViewById(R.id.txtSubject);
        TextView txtDesc = (TextView) view.findViewById(R.id.txtDesc);

        txtType.setText(content_details.type);
        txtSubject.setText(content_details.subject);
        txtDesc.setText(content_details.description);

        return view;
    }
}
