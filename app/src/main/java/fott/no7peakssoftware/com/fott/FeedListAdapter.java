package fott.no7peakssoftware.com.fott;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

import fott.no7peakssoftware.com.fott.R;
import fott.no7peakssoftware.com.fott.model.Content;
import fott.no7peakssoftware.com.fott.utils.DeviceUtils;

/**
 * Created by Itee on 17/02/2015.
 */
public class FeedListAdapter extends ArrayAdapter<Content> {

    private Context context;
    private LayoutInflater inflater;
    private ArrayList<Content> contentList;

    public FeedListAdapter(Context context, ArrayList<Content> objects) {
        super(context, 0, objects);
        this.context = context;
        this.contentList = objects;
    }

    @Override
    public int getCount() {
        return contentList.size();
    }

    @Override
    public Content getItem(int position) {
        return contentList.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        inflater = LayoutInflater.from(context);
        View view = convertView;
        ViewHolder holder;

        DeviceUtils deviceUtils = new DeviceUtils(context);
        int h = deviceUtils.getDisplayHeight();

        if(view == null) {
            view = inflater.inflate(R.layout.layout_list_item, parent, false);
            holder = new ViewHolder();

            AbsListView.LayoutParams layoutParam = new AbsListView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT
                    , h / 2);
            view.setLayoutParams(layoutParam);

            holder.imgThumbnail = (ImageView) view.findViewById(R.id.imgThumbnail);
            holder.txtTitle = (TextView) view.findViewById(R.id.txtTitle);
            holder.txtAuthor = (TextView) view.findViewById(R.id.txtAuthor);
            holder.txtDesc = (TextView) view.findViewById(R.id.txtDesc);

            view.setTag(holder);
        } else {

            holder = (ViewHolder) view.getTag();
        }

        ImageLoader.getInstance().displayImage(contentList.get(position).image, holder.imgThumbnail);

        holder.txtTitle.setText(contentList.get(position).title);
        holder.txtAuthor.setText(contentList.get(position).dateTime);
        holder.txtDesc.setText(contentList.get(position).ingress);

        return view;
    }

    class ViewHolder {
        ImageView imgThumbnail;
        TextView txtTitle, txtAuthor, txtDesc;
    }
}