package fott.no7peakssoftware.com.fott;


import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.jeremyfeinstein.slidingmenu.lib.app.SlidingFragmentActivity;

import fott.no7peakssoftware.com.fott.fragment.FragmentSlideMenu;
import fott.no7peakssoftware.com.fott.model.Response;
import fott.no7peakssoftware.com.fott.utils.GetDataFromUrl;


public class MainActivity extends SlidingFragmentActivity implements SwipeRefreshLayout.OnRefreshListener {

    public static final String KEY_RESPONSE = "response";

    private Context context;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ListView listView;

    private Fragment mFrag;
    private ViewGroup viewGroup;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_ACTION_BAR);
        setContentView(R.layout.activity_main);

        context = MainActivity.this;

        getActionBar().setCustomView(R.layout.custom_topbar);
        getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);

        viewGroup = (ViewGroup) getActionBar().getCustomView();
        ImageView btnMenu = (ImageView) viewGroup.findViewById(R.id.btnMenu);
        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggle();
            }
        });

        if (savedInstanceState == null) {
            FragmentTransaction t = this.getSupportFragmentManager().beginTransaction();
            mFrag = new FragmentSlideMenu();
            t.replace(R.id.menu_frame, mFrag);
            t.commit();
        } else {
            mFrag = (Fragment)this.getSupportFragmentManager().findFragmentById(R.id.menu_frame);
        }

        setBehindContentView(R.layout.menu_frame);

        SlidingMenu sm = getSlidingMenu();
        sm.setShadowWidthRes(R.dimen.shadow_width);
        sm.setShadowDrawable(R.drawable.shadow);
        sm.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        sm.setFadeDegree(0.35f);
        sm.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorScheme(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        listView = (ListView) findViewById(R.id.listView);

        Intent bundle = getIntent();
        final Response response = (Response) bundle.getSerializableExtra(KEY_RESPONSE);

        listView.setAdapter(new FeedListAdapter(context, response.content));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(context, DetailsActivity.class);
                intent.putExtra(DetailsActivity.KEY_CONTENT, response.content.get(position));
                startActivity(intent);
            }
        });
    }

    @Override
    public void onRefresh() {

        new GetDataFromUrl(context, listView, swipeRefreshLayout).execute();
    }


}
