package fott.no7peakssoftware.com.fott;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import fott.no7peakssoftware.com.fott.model.Response;
import fott.no7peakssoftware.com.fott.utils.FileHelper;
import fott.no7peakssoftware.com.fott.utils.HttpUtils;
import fott.no7peakssoftware.com.fott.utils.NetworkUtils;

public class SplashActivity extends Activity {

    private Context context;
    private FileHelper fileHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splash);

        context = SplashActivity.this;
        fileHelper = new FileHelper(context);

        NetworkUtils networkUtils = new NetworkUtils(context);
        if(networkUtils.isNetworkAvailable()) {
            Log.d("DEV","Network available");

            if(networkUtils.isOnline()){
                Log.d("DEV","Network is online");
                Toast.makeText(context, "Loading content, please wait a second...", Toast.LENGTH_SHORT).show();

                new GetDataFromUrl().execute();
            }
        } else {
            Log.d("DEV","Offline");

            Toast.makeText(context, "Offline mode, please check your internet connection!", Toast.LENGTH_SHORT).show();

            final Response response = fileHelper.readObjectFromFile();

            new Thread(new Runnable() {
                public void run() {
                    try {
                        Thread.sleep(3000);
                    } catch (InterruptedException e) { }

                    Intent intent = new Intent(context, MainActivity.class);
                    intent.putExtra(MainActivity.KEY_RESPONSE, response);
                    startActivity(intent);
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                }
            }).start();
        }

    }

    private class GetDataFromUrl extends AsyncTask<Void, Void, String>{

        String url = "http://87.251.89.41/application/11424/article/get_articles_list";

        @Override
        protected String doInBackground(Void... params) {
            HttpUtils httpUtils = new HttpUtils();
            return httpUtils.getJSONFromUrl(url);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {

                JSONObject jsonObject = new JSONObject(result);
                Response response = new Response(jsonObject);

                fileHelper.storeObjectToFile(response);

                Intent intent = new Intent(context, MainActivity.class);
                intent.putExtra(MainActivity.KEY_RESPONSE, response);
                startActivity(intent);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

            } catch (JSONException e) {

                Toast.makeText(context, "Loading data failed...", Toast.LENGTH_SHORT).show();
            }

        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        finish();
    }
}
