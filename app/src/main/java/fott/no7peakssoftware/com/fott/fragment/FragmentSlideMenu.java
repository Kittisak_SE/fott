package fott.no7peakssoftware.com.fott.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import fott.no7peakssoftware.com.fott.MainActivity;
import fott.no7peakssoftware.com.fott.R;

/**
 * Created by Itee on 17/02/2015.
 */
public class FragmentSlideMenu extends Fragment {

    private Context context;
    private ListView listView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_slide_menu, container, false);
        listView = (ListView) view.findViewById(R.id.listView);
        listView.setDivider(null);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final ArrayList<String> list = new ArrayList<>();
        list.add("FOTT FEED");
        list.add("Trips");
        list.add("People");

        listView.setAdapter(new MenuListAdapter(list));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(position == 0) {
                    ((MainActivity) getActivity()).toggle();
                }

                Toast.makeText(context, "" + list.get(position) + " Click", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private class MenuListAdapter extends ArrayAdapter<String> {

        private ArrayList<String> stringList;
        private LayoutInflater inflater;

        public MenuListAdapter(ArrayList<String> objects) {
            super(context, 0, objects);
            this.stringList = objects;
            inflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return stringList.size();
        }

        @Override
        public String getItem(int position) {
            return stringList.get(position);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            ViewHolder holder;

            if(view == null){
                view = inflater.inflate(R.layout.layout_menu_item, parent, false);
                holder = new ViewHolder();

                holder.txtTitle = (TextView) view.findViewById(R.id.txtTitle);

                view.setTag(holder);
            } else {

                holder = (ViewHolder) view.getTag();
            }

            holder.txtTitle.setText(stringList.get(position));

            return view;
        }

        class ViewHolder {
            TextView txtTitle;
        }
    }
}
