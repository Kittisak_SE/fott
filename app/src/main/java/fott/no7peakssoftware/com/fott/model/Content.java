package fott.no7peakssoftware.com.fott.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Itee on 17/02/2015.
 */
public class Content implements Serializable{

    private static final String KEY_ID = "id";
    private static final String KEY_TITLE = "title";
    private static final String KEY_DATETIME = "dateTime";
    private static final String KEY_TAGS = "tags";
    private static final String KEY_CONTENT = "content";
    private static final String KEY_INGRESS = "ingress";
    private static final String KEY_IMAGE = "image";
    private static final String KEY_CREATED = "created";
    private static final String KEY_CHANGED = "changed";

    public String id, title, dateTime, ingress, image, created, changed;
    public ArrayList<Content_Details> contentList = new ArrayList<>();
    public ArrayList<String> tagList = new ArrayList<>();

    public Content(JSONObject jsonObject) {

        try {
            id = jsonObject.getString(KEY_ID);
            title = jsonObject.getString(KEY_TITLE);
            dateTime = jsonObject.getString(KEY_DATETIME);
            ingress = jsonObject.getString(KEY_INGRESS);
            image = jsonObject.getString(KEY_IMAGE);
            created = jsonObject.getString(KEY_CREATED);
            changed = jsonObject.getString(KEY_CHANGED);

            JSONArray tagArr = jsonObject.getJSONArray(KEY_TAGS);
            for (int i = 0; i < tagArr.length(); i++) {
                String newTag = tagArr.getString(i);
                tagList.add(newTag);
            }

            JSONArray contentArr = jsonObject.getJSONArray(KEY_CONTENT);
            for (int i = 0; i < contentArr.length(); i++) {
                Content_Details newContent = new Content_Details(contentArr.getJSONObject(i));
                contentList.add(newContent);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}


