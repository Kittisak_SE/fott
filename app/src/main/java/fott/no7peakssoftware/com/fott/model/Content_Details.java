package fott.no7peakssoftware.com.fott.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by Itee on 17/02/2015.
 */
public class Content_Details implements Serializable {

    private static final String KEY_TYPE = "type";
    private static final String KEY_SUBJECT = "subject";
    private static final String KEY_DESCRIPTION = "description";

    public String type, subject, description;

    public Content_Details(JSONObject jsonObject) {

        try {
            type = jsonObject.getString(KEY_TYPE);
            subject = jsonObject.getString(KEY_SUBJECT);
            description = jsonObject.getString(KEY_DESCRIPTION);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
