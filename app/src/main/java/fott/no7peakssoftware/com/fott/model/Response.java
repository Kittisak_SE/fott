package fott.no7peakssoftware.com.fott.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Itee on 17/02/2015.
 */
public class Response implements Serializable {

    private static final String KEY_STATUS = "status";
    private static final String KEY_CONTENT = "content";
    private static final String KEY_ServerTime = "serverTime";

    public String status, serverTime;
    public ArrayList<Content> content = new ArrayList<>();

    public Response(JSONObject jsonObject) {

        try {
            status = jsonObject.getString(KEY_STATUS);
            serverTime = jsonObject.getString(KEY_ServerTime);

            JSONArray contentArr = jsonObject.getJSONArray(KEY_CONTENT);
            for(int i = 0; i < contentArr.length(); i++){
                Content newContent = new Content(contentArr.getJSONObject(i));
                content.add(newContent);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
