package fott.no7peakssoftware.com.fott.utils;

import android.content.Context;
import android.view.Display;

/**
 * Created by Itee on 17/02/2015.
 */
public class DeviceUtils {

    private Context context;

    public DeviceUtils(Context context){
        this.context = context;
    }

    public int getDisplayWidth() {
        return context.getResources().getDisplayMetrics().widthPixels;
    }

    public int getDisplayHeight() {
        return context.getResources().getDisplayMetrics().heightPixels;
    }
}
