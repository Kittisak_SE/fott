package fott.no7peakssoftware.com.fott.utils;

import android.content.Context;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;

import fott.no7peakssoftware.com.fott.model.Response;

/**
 * Created by Itee on 17/02/2015.
 */
public class FileHelper {

    private static final String RESPONSE_DATA_FILE = "response.dat";
    private Context context;

    public FileHelper(Context context){
        this.context = context;
    }

    private static String externalDirectoryPath(String directoryName, Context context){
        return context.getExternalFilesDir(null) + "/" + directoryName;
    }

    public boolean fileExists() {
        File file = context.getFileStreamPath(RESPONSE_DATA_FILE);
        return file.exists();
    }

    public boolean fileExist(String directoryName, Context context){
        File file = new File(externalDirectoryPath(directoryName, context), RESPONSE_DATA_FILE);
        return file.exists();
    }

    public void deleteFile(){
        String dir = context.getFilesDir().getAbsolutePath();
        File file = new File(dir, RESPONSE_DATA_FILE);
        boolean d0 = file.delete();
        Log.w("DEV", "File deleted: " + dir + "/myFile " + d0);
    }

    public void storeObjectToFile(Response object) {

        if(fileExists())
            deleteFile();
        try {
            FileOutputStream fos = context.openFileOutput(RESPONSE_DATA_FILE, Context.MODE_PRIVATE);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(object);
            os.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public Response readObjectFromFile() {
        try {
            FileInputStream fis = context.openFileInput(RESPONSE_DATA_FILE);
            ObjectInputStream is = new ObjectInputStream(fis);
            Response object = (Response) is.readObject();
            is.close();
            return object;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (StreamCorruptedException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }
}
