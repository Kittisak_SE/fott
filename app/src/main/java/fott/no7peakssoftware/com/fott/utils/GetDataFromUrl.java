package fott.no7peakssoftware.com.fott.utils;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.widget.SwipeRefreshLayout;
import android.widget.ListView;

import org.json.JSONException;
import org.json.JSONObject;

import fott.no7peakssoftware.com.fott.FeedListAdapter;
import fott.no7peakssoftware.com.fott.model.Response;

/**
 * Created by Itee on 17/02/2015.
 */
public class GetDataFromUrl  extends AsyncTask<Void, Void, String> {

    String url = "http://87.251.89.41/application/11424/article/get_articles_list";

    private Context context;
    private ListView listView;
    private SwipeRefreshLayout swipeRefreshLayout;

    public GetDataFromUrl(Context context, ListView listView, SwipeRefreshLayout swipeRefreshLayout){
        this.context = context;
        this.listView = listView;
        this.swipeRefreshLayout = swipeRefreshLayout;
    }

    @Override
    protected String doInBackground(Void... params) {
        HttpUtils httpUtils = new HttpUtils();
        return httpUtils.getJSONFromUrl(url);
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        if(swipeRefreshLayout !=null) {
            swipeRefreshLayout.setRefreshing(false);
        }

        try {

            JSONObject jsonObject = new JSONObject(result);
            Response response = new Response(jsonObject);

            listView.setAdapter(new FeedListAdapter(context, response.content));

        } catch (JSONException e){

        }

    }
}
